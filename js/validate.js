
jQuery.validator.addMethod("accept", function(value, element, param) {
    return value.match(new RegExp("." + param + "$"));
  });
// Wait for the DOM to be ready
$(function() {
    // Init form validation on theform.
    // It has the name attribute "formular"
    $("form[name='formular']").validate({
      // Specify validation rules
      rules: {
        name: {
            required:true,
            accept: "[a-zA-Z]+"
        },
        email: {
          required: true,
          // Specify that email should be validated
          // by the built-in "email" rule
          email: true
        },
        komentar: "required",
        telefon:{
            required:false,
            number:true


        }
      },
      // Specify validation error messages
      messages: {
        name: {
            required: "Please enter your name",
            accept: "Letters only please"
        },
        telefon: {
            accept: "Letters only please"
        },
        email: "Please enter a valid email address",
        komentar: "Please enter komentar"
      },
      errorElement : 'div',
      errorLabelContainer: '.error',
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
        form.submit();
      }
    });
  });