module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    grunt.initConfig({

      less: {
        development: {
          options: {
            compress: true,
            yuicompress: true,
            optimization: 2
          },
          files: {
            "css/style.css": "less/style.less" // final file & source file
          }
        }
      },
      watch: {
        styles: {
          files: ['less/**/*.less'], // files to watch
          tasks: ['less'],
          options: {
            nospawn: true
          }
        }
      },
      copy: {
        build: {
          cwd: '',
          src: [ '**' ],
          dest: 'build',
          expand: true
        },
      },
    clean: {
    build: {
    src: [ 'build' ]
  },
},
    });
    grunt.registerTask(
        'build', 
        'Compiles all of the assets and copies the files to the build directory.', 
        [ 'clean', 'copy' ]
      );
      
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.registerTask('default', ['less', 'watch']);
  };
  